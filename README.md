# Hactar RAG Chatbot

Hactar RAG is a chatbot that uses the RAG (Retrieval-Augmented Generation) model to answer questions about PDF files. The chatbot uses UnstructuredPDF to extract the text from the PDF files, then embeds the text using a sentence embedding model (e.g. `mxbai-embed-large`) and stores the embeddings in a vector store. When a question is asked, the chatbot retrieves the most similar embeddings from the vector store and uses the RAG model to generate an answer.

The UI is powered by Taipy, and the backend uses Ollama to interact with the embedding model and the RAG model.

## How to run Hactar RAG

### Using Docker

There is a Dockerfile and a docker-compose.yml file in the root directory of the project. To run the project using Docker, you can use the following commands:

#### Build the Docker image

```bash
docker compose build
```

#### Run the Docker container

In the `docker-compose.yml` file, some environment variables are defined. You can change the values of these variables in the file before running the container:

- `OLLAMA_API`: The URL of the OLLAMA API.
- `EMBEDDING_MODEL`: The name of the embedding model to use.
- `LLM_MODEL`: The name of the LLM model to use.

You also have to fill the `pdfs` directory with the PDF files you want to use for the chatbot.

After changing the values of the environment variables, you can run the container using the following command:

```bash
docker compose up
```
